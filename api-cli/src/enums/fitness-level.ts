export enum FitnessLevel {
    Beginner = 1,
    Intermediate = 2,
    Advanced = 3,
}
