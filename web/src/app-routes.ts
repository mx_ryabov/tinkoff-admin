export enum AppRoutes {
    Root = '/',
    Workers = '/workers',
    Products = '/products',
}
