export enum DocumentType {
    TermsAndConditions = 1,
    PrivacyPolicy = 2,
    LiabilityWaiver = 3,
}
