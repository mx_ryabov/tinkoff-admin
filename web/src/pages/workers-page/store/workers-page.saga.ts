import { call, takeLatest, put } from '@redux-saga/core/effects';
import { SagaIterator } from '@redux-saga/types';
import { WorkersApi } from '@tinkoff-admin/api';
import {
    GetWorkers,
    GetWorkersError,
    GetWorkersSuccess,
    RemoveWorker,
    RemoveWorkerError,
    RemoveWorkerSuccess,
} from './workers-page.actions';

function* getWorkersHandler() {
    const { data, error } = yield call(WorkersApi.getWorkers);

    if (!error) {
        yield put({ ...new GetWorkersSuccess(data.data) });
    } else {
        yield put({ ...new GetWorkersError(error) });
    }
}

function* removeWorkerHandler(action: RemoveWorker) {
    const { error } = yield call(WorkersApi.removeWorker, action.person_id);

    if (!error) {
        yield put({ ...new RemoveWorkerSuccess() });
        yield call(getWorkersHandler);
    } else {
        yield put({ ...new RemoveWorkerError(error) });
    }
}

export default function* workersPageSaga(): SagaIterator {
    yield takeLatest(GetWorkers.Name, getWorkersHandler);
    yield takeLatest(RemoveWorker.Name, removeWorkerHandler);
}
