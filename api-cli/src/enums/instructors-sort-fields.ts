export enum InstructorsSortField {
    InstructorId = 1,
    IsAdmin = 2,
    Name = 3,
    Order = 4,
}
