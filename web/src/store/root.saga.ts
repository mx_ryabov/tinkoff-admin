import { all, spawn } from 'redux-saga/effects';
import productsPageSaga from '../pages/products-page/store/products-page.saga';
import workersPageSaga from '../pages/workers-page/store/workers-page.saga';

export default function* rootSaga() {
    yield all([spawn(workersPageSaga), spawn(productsPageSaga)]);
}
