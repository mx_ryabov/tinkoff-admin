import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { AppRoutes } from './app-routes';
import ProductsPage from './pages/products-page/products-page.component';
import WorkersPage from './pages/workers-page/workers-page.component';

const App: React.FC = () => {
    return (
        <Switch>
            <Route exact path={AppRoutes.Workers} component={WorkersPage} />
            <Route exact path={AppRoutes.Products} component={ProductsPage} />
            <Redirect from={AppRoutes.Root} to={AppRoutes.Workers} />
        </Switch>
    );
};

export default App;
