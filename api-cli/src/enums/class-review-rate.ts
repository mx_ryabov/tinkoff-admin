export enum ClassReviewRate {
    NeedsImprovement = 1,
    OK = 2,
    Excellent = 3,
}

export const ClassReviewRateNames = {
    [ClassReviewRate.NeedsImprovement]: '1 - Needs Improvement',
    [ClassReviewRate.OK]: '2 - OK',
    [ClassReviewRate.Excellent]: '3 - Excellent',
};