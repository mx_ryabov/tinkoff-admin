import { call, takeLatest, put } from '@redux-saga/core/effects';
import { SagaIterator } from '@redux-saga/types';
import { ProductsApi } from '@tinkoff-admin/api';
import {
    GetProducts,
    GetProductsSuccess,
    GetProductsError,
    RemoveProduct,
    RemoveProductSuccess,
    RemoveProductError,
} from './products-page.actions';

function* getProductsHandler() {
    const { data, error } = yield call(ProductsApi.getProducts);

    if (!error) {
        yield put({ ...new GetProductsSuccess(data.data) });
    } else {
        yield put({ ...new GetProductsError(error) });
    }
}

function* removeProductHandler(action: RemoveProduct) {
    const { error } = yield call(ProductsApi.removeProduct, action.product_id);

    if (!error) {
        yield put({ ...new RemoveProductSuccess() });
        yield call(getProductsHandler);
    } else {
        yield put({ ...new RemoveProductError(error) });
    }
}

export default function* productsPageSaga(): SagaIterator {
    yield takeLatest(GetProducts.Name, getProductsHandler);
    yield takeLatest(RemoveProduct.Name, removeProductHandler);
}
