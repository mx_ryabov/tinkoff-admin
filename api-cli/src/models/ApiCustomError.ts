import { ApiStatusCode } from '../enums/api-status-code';

export interface ApiCustomError {
    apiStatusCode: ApiStatusCode;
    error: string;
}
