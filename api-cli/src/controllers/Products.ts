import { Product, ResponseArray } from '..';
import Api from '../utils/api';

export class ProductsApi {
    static getProducts() {
        return Api.get<ResponseArray<Product>>('/faq/products/');
    }

    static removeProduct(product_id: number) {
        return Api.delete(`/faq/products/${product_id}`);
    }
}
