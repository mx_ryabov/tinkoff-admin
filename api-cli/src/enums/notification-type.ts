export enum NotificationType {
    ClassInvitations = 1,
    ConnectionRequests = 2,
    ReviewClass = 3,
    Cheer = 4,
    Comment = 5,
    Milestone = 6,
    Instructor = 7,
}
