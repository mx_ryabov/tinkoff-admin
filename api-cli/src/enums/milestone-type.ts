export enum MilestoneType {
    Class1th = 1,
    Weekly1stStreak = 2,
    Week4Streak = 3,
    Class50th = 4,
    Class100th = 5,
}
