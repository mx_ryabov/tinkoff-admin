import { Box, CircularProgress, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@mui/material';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store/root.reducer';
import { GetWorkers, RemoveWorker } from '../workers-page/store/workers-page.actions';
import AdminLayout from '../../components/layouts/admin-layout/admin-layout.component';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';

const WorkersPage: React.FC = () => {
    const dispatch = useDispatch();
    const workersState = useSelector((rootState: RootState) => rootState.workersPage.workers);

    useEffect(() => {
        dispatch({ ...new GetWorkers() });
    }, [dispatch]);

    useEffect(() => {
        console.log(workersState);
    }, [workersState]);

    const removeWorker = (person_id: number) => {
        dispatch({ ...new RemoveWorker(person_id) });
    };

    return (
        <AdminLayout>
            <Box sx={{ paddingTop: 4 }}>
                <Typography variant="h4" sx={{ marginBottom: 4 }}>
                    Управление списком сотрудников ТЦР Екатеринбург
                </Typography>
                {workersState.loading && (
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                        <CircularProgress />
                    </Box>
                )}
                {!workersState.loading && (
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Фамилия</TableCell>
                                <TableCell>Имя</TableCell>
                                <TableCell>Должность</TableCell>
                                <TableCell>Статус</TableCell>
                                <TableCell>Действия</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {workersState.data.map((worker) => (
                                <TableRow
                                    key={worker.person_id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell>{worker.surname}</TableCell>
                                    <TableCell>{worker.name}</TableCell>
                                    <TableCell>{worker.function_name}</TableCell>
                                    <TableCell>{worker.status ? 'Активный' : 'Удален'}</TableCell>
                                    <TableCell>
                                        <DeleteOutlinedIcon onClick={() => removeWorker(worker.person_id)} />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                )}
            </Box>
        </AdminLayout>
    );
};

export default WorkersPage;
