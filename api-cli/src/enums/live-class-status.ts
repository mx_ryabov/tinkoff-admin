
export enum LiveClassStatus {
    NoShow = 1,    
    CheckIn = 2,
    Booked = 3,
    Cancelled = 4,
}

export const LiveClassStatusNames = {    
    [LiveClassStatus.Booked]: 'Booked',
    [LiveClassStatus.NoShow]: 'No Show',
    [LiveClassStatus.Cancelled]: 'Cancelled',
    [LiveClassStatus.CheckIn]: 'Check In',
};


