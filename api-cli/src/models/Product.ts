export interface Product {
    id: number;
    product_name: string;
    category_name: string;
    price: string;
    shop_name: string;
    shop_location: string;
    count: number;
}
