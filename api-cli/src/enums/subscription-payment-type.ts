﻿export enum SubscriptionPaymentType {
    None,
    Stripe,
    Apple,
    Android,
}
