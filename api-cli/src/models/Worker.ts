export interface Worker {
    person_id: number;
    name: string;
    surname: string;
    email: string;
    function_name: string;
    about: string;
    status: boolean;
}
