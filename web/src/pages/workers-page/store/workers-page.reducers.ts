import { AxiosError, Worker } from '@tinkoff-admin/api';
import {
    GetWorkers,
    GetWorkersError,
    GetWorkersSuccess,
    RemoveWorker,
    RemoveWorkerError,
    RemoveWorkerSuccess,
    WorkersPageActions,
} from './workers-page.actions';

export interface WorkersPageState {
    workers: {
        loading: boolean;
        data: Worker[];
        error?: AxiosError;
    };
    removeWorker: {
        loading: boolean;
        error?: AxiosError;
    };
}

const initialState: WorkersPageState = {
    workers: {
        loading: false,
        data: [],
        error: undefined,
    },
    removeWorker: {
        loading: false,
    },
};

export function workersPageReducer(state = initialState, action: WorkersPageActions): WorkersPageState {
    switch (action.type) {
        case GetWorkers.Name: {
            return {
                ...state,
                workers: {
                    ...state.workers,
                    loading: true,
                    error: undefined,
                },
            };
        }

        case GetWorkersSuccess.Name: {
            return {
                ...state,
                workers: {
                    ...state.workers,
                    loading: false,
                    data: action.data,
                },
            };
        }

        case GetWorkersError.Name: {
            return {
                ...state,
                workers: {
                    ...state.workers,
                    loading: false,
                    error: action.error,
                },
            };
        }

        case RemoveWorker.Name: {
            return {
                ...state,
                removeWorker: {
                    ...state.removeWorker,
                    loading: true,
                    error: undefined,
                },
            };
        }

        case RemoveWorkerSuccess.Name: {
            return {
                ...state,
                removeWorker: {
                    ...state.removeWorker,
                    loading: false,
                },
            };
        }

        case RemoveWorkerError.Name: {
            return {
                ...state,
                removeWorker: {
                    ...state.removeWorker,
                    loading: false,
                    error: action.error,
                },
            };
        }

        default: {
            return state;
        }
    }
}
