import { Action } from 'redux';
import { AxiosError, Worker } from '@tinkoff-admin/api';

export class GetWorkers implements Action {
    public static readonly Name = '[Workers Page] GetWorkers';
    readonly type = GetWorkers.Name;
}

export class GetWorkersSuccess implements Action {
    public static readonly Name = '[Workers Page] GetWorkersSuccess';
    readonly type = GetWorkersSuccess.Name;
    constructor(public data: Worker[]) {}
}

export class GetWorkersError implements Action {
    public static readonly Name = '[Workers Page] GetWorkersError';
    readonly type = GetWorkersError.Name;
    constructor(public error: AxiosError) {}
}

export class RemoveWorker implements Action {
    public static readonly Name = '[Workers Page] RemoveWorker';
    readonly type = RemoveWorker.Name;
    constructor(public person_id: number) {}
}

export class RemoveWorkerSuccess implements Action {
    public static readonly Name = '[Workers Page] RemoveWorkerSuccess';
    readonly type = RemoveWorkerSuccess.Name;
}

export class RemoveWorkerError implements Action {
    public static readonly Name = '[Workers Page] RemoveWorkerError';
    readonly type = RemoveWorkerError.Name;
    constructor(public error: AxiosError) {}
}

export type WorkersPageActions =
    | GetWorkers
    | GetWorkersSuccess
    | GetWorkersError
    | RemoveWorker
    | RemoveWorkerSuccess
    | RemoveWorkerError;
