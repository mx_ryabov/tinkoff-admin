export enum SubscriptionStatus {
    AddedToCart = 1,
    Booked = 2,
    Cancelled = 3,
}
