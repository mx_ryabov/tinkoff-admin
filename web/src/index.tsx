import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { applyMiddleware, compose, createStore } from 'redux';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { createRootReducer } from './store/root.reducer';
import { ConnectedRouter, routerMiddleware } from 'connected-react-router';
// import { composeWithDevTools } from 'redux-devtools-extension';
import { initAxios, AxiosError } from '@tinkoff-admin/api';
import rootSaga from './store/root.saga';

export const history = createBrowserHistory();

const env = (window as any)._env_;
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
    createRootReducer(history),
    compose(applyMiddleware(routerMiddleware(history), sagaMiddleware))
);

initAxios(env.API_URL, (error: AxiosError) => {
    if (401 === error.response?.status) {
        console.error('Auth failed');
    }
});

sagaMiddleware.run(rootSaga);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <App />
            </ConnectedRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
