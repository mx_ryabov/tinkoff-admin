import { AxiosError, Product } from '@tinkoff-admin/api';
import {
    ProductsPageActions,
    GetProducts,
    GetProductsError,
    GetProductsSuccess,
    RemoveProduct,
    RemoveProductSuccess,
    RemoveProductError,
} from './products-page.actions';

export interface ProductsPageState {
    products: {
        loading: boolean;
        data: Product[];
        error?: AxiosError;
    };
    removeProduct: {
        loading: boolean;
        error?: AxiosError;
    };
}

const initialState: ProductsPageState = {
    products: {
        loading: false,
        data: [],
        error: undefined,
    },
    removeProduct: {
        loading: false,
    },
};

export function productsPageReducer(state = initialState, action: ProductsPageActions): ProductsPageState {
    switch (action.type) {
        case GetProducts.Name: {
            return {
                ...state,
                products: {
                    ...state.products,
                    loading: true,
                    error: undefined,
                },
            };
        }

        case GetProductsSuccess.Name: {
            return {
                ...state,
                products: {
                    ...state.products,
                    loading: false,
                    data: action.data,
                },
            };
        }

        case GetProductsError.Name: {
            return {
                ...state,
                products: {
                    ...state.products,
                    loading: false,
                    error: action.error,
                },
            };
        }

        case RemoveProduct.Name: {
            return {
                ...state,
                removeProduct: {
                    ...state.removeProduct,
                    loading: true,
                    error: undefined,
                },
            };
        }

        case RemoveProductSuccess.Name: {
            return {
                ...state,
                removeProduct: {
                    ...state.removeProduct,
                    loading: false,
                },
            };
        }

        case RemoveProductError.Name: {
            return {
                ...state,
                removeProduct: {
                    ...state.removeProduct,
                    loading: false,
                    error: action.error,
                },
            };
        }

        default: {
            return state;
        }
    }
}
