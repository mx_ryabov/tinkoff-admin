import { ResponseArray } from '..';
import Api from '../utils/api';

export class WorkersApi {
    static getWorkers() {
        return Api.get<ResponseArray<Worker>>('/workers/');
    }

    static removeWorker(person_id: number) {
        console.log(person_id);

        return Api.delete(`/workers/${person_id}`);
    }
}
