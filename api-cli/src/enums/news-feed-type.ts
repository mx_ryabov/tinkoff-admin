export enum NewsFeedType {
    Post = 1, //Post(writing/displaying)
    OnDemand = 2, // VOD
    ScheduleReview = 3, // Class review
    ScheduleBooked = 4, // Booked class
    SchedulePopular = 5,
    InstructorReview = 6,
    DefaultPost = 7,
    Milestone = 8,
}
