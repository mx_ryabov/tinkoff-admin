export enum VideoServiceType {
    Twilio = 1,
    Zoom = 2,
    Agora = 3,
    ZoomSDK = 4,
}
