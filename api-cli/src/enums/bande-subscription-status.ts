﻿export enum BandeSubscriptionStatus {
    None = 0,
    Active = 1,
    Inactive = 2,
    Trial = 3,
    Canceled = 4,
}
