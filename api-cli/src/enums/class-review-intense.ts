export enum ClassReviewIntense {
    HarderThanExpected = 1,
    JustRight = 2,
    EasierThanExpected = 3,
}

export const ClassReviewIntenseNames = {
    [ClassReviewIntense.HarderThanExpected]: 'Harder Than Expected',
    [ClassReviewIntense.JustRight]: 'Just Right',
    [ClassReviewIntense.EasierThanExpected]: 'Easier Than Expected',
};