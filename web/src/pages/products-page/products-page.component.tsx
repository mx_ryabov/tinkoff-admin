import { Box, CircularProgress, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@mui/material';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AdminLayout from '../../components/layouts/admin-layout/admin-layout.component';
import { RootState } from '../../store/root.reducer';
import { GetProducts, RemoveProduct } from './store/products-page.actions';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';

const ProductsPage: React.FC = () => {
    const dispatch = useDispatch();
    const productsState = useSelector((rootState: RootState) => rootState.productsPage.products);

    useEffect(() => {
        dispatch({ ...new GetProducts() });
    }, [dispatch]);

    useEffect(() => {
        console.log(productsState);
    }, [productsState]);

    const removeProduct = (product_id: number) => {
        dispatch({ ...new RemoveProduct(product_id) });
    };

    return (
        <AdminLayout>
            <Box sx={{ paddingTop: 4 }}>
                <Typography variant="h4" sx={{ marginBottom: 4 }}>
                    Управление списком продуктов в ТЦР Екатеринбург
                </Typography>
                {productsState.loading && (
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                        <CircularProgress />
                    </Box>
                )}
                {!productsState.loading && (
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Название</TableCell>
                                <TableCell>Цена</TableCell>
                                <TableCell>Количество</TableCell>
                                <TableCell>Магазин</TableCell>
                                <TableCell>Действия</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {productsState.data.map((product) => (
                                <TableRow key={product.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                    <TableCell>{product.product_name}</TableCell>
                                    <TableCell>{product.price}</TableCell>
                                    <TableCell>{product.count}</TableCell>
                                    <TableCell>{product.shop_name}</TableCell>
                                    <TableCell>
                                        <DeleteOutlinedIcon onClick={() => removeProduct(product.id)} />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                )}
            </Box>
        </AdminLayout>
    );
};

export default ProductsPage;
