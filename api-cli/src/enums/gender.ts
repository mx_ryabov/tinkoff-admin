export enum Gender {
    Male = 1,
    Female = 2,
    NonBinary = 3,
    Unspecified = 4,
}


export const GenderNames = {
    [Gender.Male]: "Male",
    [Gender.Female]: "Female",
    [Gender.NonBinary]: "Non-binary",
    [Gender.Unspecified]: "Unspecified"
};