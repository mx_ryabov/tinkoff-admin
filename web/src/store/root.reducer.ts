import { combineReducers } from 'redux';
import { History } from 'history';
import { RouterState, connectRouter } from 'connected-react-router';
import { workersPageReducer, WorkersPageState } from '../pages/workers-page/store/workers-page.reducers';
import { productsPageReducer, ProductsPageState } from '../pages/products-page/store/products-page.reducers';

export interface RootState {
    router: RouterState;
    workersPage: WorkersPageState;
    productsPage: ProductsPageState;
}

export const createRootReducer = (history: History) =>
    combineReducers<RootState>({
        router: connectRouter(history),
        workersPage: workersPageReducer,
        productsPage: productsPageReducer,
    });
