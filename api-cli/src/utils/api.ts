import axios, { AxiosInstance, AxiosResponse, AxiosError } from 'axios';
import { ApiCustomError } from '../models/ApiCustomError';

let Api: AxiosInstance = axios.create();

export interface ApiResponse<T> {
    data: T;
    error?: AxiosError;
}

export const initAxios = (apiUrl: string, errorHandler: (error: AxiosError) => void) => {
    Api.defaults.baseURL = apiUrl;
    Api.defaults.responseType = 'json';

    Api.interceptors.response.use(
        (response: AxiosResponse) => response,
        (error: AxiosError) => {
            errorHandler && errorHandler(error);
            return Promise.resolve({ error });
        }
    );
};

export const clearAuthHeaders = () => {
    delete Api.defaults.headers.common['Authorization'];
    delete Api.defaults.headers.common['AdminAuthorization'];
};

export const setAuthHeaders = (token: string, adminToken?: string) => {
    clearAuthHeaders();
    if (token) {
        Api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }
    if (adminToken) {
        Api.defaults.headers.common['AdminAuthorization'] = `Bearer ${adminToken}`;
    }
};

const API_CUSTOM_ERROR_HTTP_STATUS_CODE = 470;
export const getApiCustomError = (error: AxiosError): ApiCustomError | undefined => {
    if (
        error.response &&
        error.response.status === API_CUSTOM_ERROR_HTTP_STATUS_CODE &&
        error.response.data &&
        error.response.data.hasOwnProperty('apiStatusCode')
    ) {
        return error.response.data as ApiCustomError;
    }
};

export default Api;
