export enum OnDemandSortFields {
    Name = 2,
    Duration,
    OrderNum,
    IsNewRelease,
    IsPublished,
}
