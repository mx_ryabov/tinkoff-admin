export enum MembersSortField {
    AccountId = 1,
    AccountRole = 2,
    Name = 3,
    Status = 4,
    IsAdmin = 5,
    FirstName = 6,
    LastName = 7,
    Gender = 8,
    Email = 9,
    Phone = 10,
    DateJoined = 11,
    LastClassOn = 12,
    SubscriptionPlanId = 13,
}