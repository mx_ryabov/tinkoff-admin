export enum PaymentType {
    Undefined = 0,
    Membership = 1,
    Card = 2,
    ApplePay = 3,
    GooglePay = 4,
}