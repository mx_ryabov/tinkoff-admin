export enum ApiStatusCode {
    Forbidden = 1,
    ZoomUserNotActive = 2,
    ScheduleDoesNotExist = 3,
    UnidentifiedError = 100,
}
