export enum Intensity {
    Light = 1,
    Moderate = 2,
    High = 3,
}
