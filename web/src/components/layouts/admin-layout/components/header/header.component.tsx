import { Container, Tabs, Tab, AppBar, Toolbar } from '@mui/material';
import Logo from '../../../../../assets/img/logo.png';
import React from 'react';
import { AppRoutes } from '../../../../../app-routes';
import { useHistory } from 'react-router-dom';

const Header: React.FC = () => {
    const history = useHistory();
    const actualRoute = history.location.pathname;

    const onTabClick = (event: any, route: AppRoutes) => {
        history.push(route);
    };
    return (
        <AppBar position="sticky" sx={{ backgroundColor: 'white' }}>
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <img src={Logo} alt="((" />
                    <Tabs value={actualRoute} onChange={onTabClick} textColor="primary" indicatorColor="primary">
                        <Tab label="Сотрудники" value={AppRoutes.Workers} />
                        <Tab label="Продукты" value={AppRoutes.Products} />
                    </Tabs>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default Header;
