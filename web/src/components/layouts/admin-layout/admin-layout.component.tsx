import { Container } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';
import Header from './components/header/header.component';

const AdminLayout: React.FC = ({ children }) => {
    return (
        <Box>
            <Header />
            <Container maxWidth="lg">{children}</Container>
        </Box>
    );
};

export default AdminLayout;
