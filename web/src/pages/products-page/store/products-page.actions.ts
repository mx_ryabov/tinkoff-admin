import { Action } from 'redux';
import { AxiosError, Product } from '@tinkoff-admin/api';

export class GetProducts implements Action {
    public static readonly Name = '[Products Page] GetProducts';
    readonly type = GetProducts.Name;
}

export class GetProductsSuccess implements Action {
    public static readonly Name = '[Products Page] GetProductsSuccess';
    readonly type = GetProductsSuccess.Name;
    constructor(public data: Product[]) {}
}

export class GetProductsError implements Action {
    public static readonly Name = '[Products Page] GetProductsError';
    readonly type = GetProductsError.Name;
    constructor(public error: AxiosError) {}
}

export class RemoveProduct implements Action {
    public static readonly Name = '[Products Page] RemoveProduct';
    readonly type = RemoveProduct.Name;
    constructor(public product_id: number) {}
}

export class RemoveProductSuccess implements Action {
    public static readonly Name = '[Products Page] RemoveProductSuccess';
    readonly type = RemoveProductSuccess.Name;
}

export class RemoveProductError implements Action {
    public static readonly Name = '[Products Page] RemoveProductError';
    readonly type = RemoveProductError.Name;
    constructor(public error: AxiosError) {}
}

export type ProductsPageActions =
    | GetProducts
    | GetProductsSuccess
    | GetProductsError
    | RemoveProduct
    | RemoveProductSuccess
    | RemoveProductError;
